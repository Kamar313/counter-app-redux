import count from "./updown";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  count,
});
export default rootReducer;
