import { legacy_createStore } from "redux";
import rootReducer from "./store";
const mainStore = legacy_createStore(rootReducer);
export default mainStore;
