import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { increment } from "./Action";
import { decrement } from "./Action";
import { reset } from "./Action";

function App() {
  const value = useSelector((state) => state.count);
  const despacth = useDispatch();
  const consition = () => {
    if (value >= 0) {
      despacth(increment());
    }
  };
  const conditionDec = () => {
    if (value > 0) {
      despacth(decrement());
    }
  };

  return (
    <>
      <div className=" text-center mt-12">
        <h1>{value}</h1>
        <button className=" border-2 mt-10 border-black" onClick={consition}>
          Increse
        </button>
        <button onClick={conditionDec} className=" border-2 border-black ml-3">
          Decrese
        </button>
        <button
          onClick={() => despacth(reset())}
          className=" border-2 border-black ml-3"
        >
          Reset
        </button>
      </div>
    </>
  );
}

export default App;
