import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import mainStore from "./Reducer/mainStore";
import { Provider } from "react-redux";
mainStore.subscribe(() => console.log(mainStore.getState()));
ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <Provider store={mainStore}>
      <App />
    </Provider>
  </React.StrictMode>
);
